<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DbController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function index(){
        try{

            $res = app('db')->select("select * from wifi_logs;");
            return json_encode(array("error" => 0, "response" => $res));
        
        }catch(\Exception $e){
            return json_encode(array("error" => 1, "response" => $e->getMessage()));
        }
    }

    public function save(Request $req){
        try{

            $id = DB::table('wifi_logs')->insert([
                'cadena' => $req->input('cadena'),
                'deviceid' => $req->input('deviceid')
            ]);
    
            return json_encode(array('error'=>0,'response'=>$id));

        }catch(\Exception $e){
            return json_encode(array('error'=>1,'response'=>$e->getMessage()));
        }
        
    }
}
